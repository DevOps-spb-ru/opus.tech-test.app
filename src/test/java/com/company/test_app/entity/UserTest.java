package com.company.test_app.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testDisplayName() {
        var user = new User();
        user.username = "n1";
        assertEquals("[n1]", user.getDisplayName());
        user.firstName = "fn1";
        user.lastName = "ln1";
        assertEquals("fn1 ln1 [n1]", user.getDisplayName());
    }

}